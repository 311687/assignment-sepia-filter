#ifndef IMAGE_UTILS
#define IMAGE_UTILS

#include <stdbool.h>

#include "image.h"

void image_make_sepia_asm(image* image);

#endif
