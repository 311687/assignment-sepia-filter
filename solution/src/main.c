#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "image.h"
#include "image_io.h"
#include "image_utils.h"

void print_help(char*);
void print_load_err(image_load_status status);
void print_save_err(image_save_status status);

int main(int arg_num, char** args) {
    if (arg_num < 3) {
        puts("Пожалуйста, укажите 2 файла");
        print_help(args[0]);
        return 0;
    }

    char* src_file = args[1];
    char* dst_file_asm = args[2];

    image* image;
    struct image* img_asm;

    image_load_status load_status = image_read_from_bmp(src_file, &image);
    image_read_from_bmp(src_file, &img_asm);

    if (load_status == IMAGE_LOAD_OK) {

        image_make_sepia_asm(img_asm);
        image_save_status save_status_asm = image_save_to_bmp(dst_file_asm, img_asm);

        if (save_status_asm == IMAGE_SAVE_OK) {
            puts("Фильтр успешно применён.");
        } else {
            print_save_err(save_status_asm);
        }
    } else {
        print_load_err(load_status);
    }

    return 0;
}

void print_load_err(image_load_status status) {
    if (status == IMAGE_LOAD_BPP_NOT_SUPPORTED)
        puts("Количество бит на пиксель не поддерживается");
    else if (status == IMAGE_LOAD_COMPRESSION_NOT_SUPPORTED)
        puts("Сжатые BMP-файлы не поддерживаются");
    else if (status == IMAGE_LOAD_FILE_NOT_EXIST)
        puts("Файл для чтения не существует");
    else if (status == IMAGE_LOAD_TYPE_UNSUPPORTED)
        puts("Неверный формат читаемого файла");
    else if (status == IMAGE_LOAD_READ_FAIL)
        puts("Не удалось прочитать файл");
    else
        puts("Ошибка.");
}

void print_save_err(image_save_status status) {
    if (status == IMAGE_SAVE_NO_ACCESS)
        puts("Нельзя запись в файл");
    else if (status == IMAGE_SAVE_OPEN_FAIL)
        puts("Не удалось открыть файл для записи");
    else if (status == IMAGE_SAVE_WRITE_FAIL)
        puts("Не удалось записать в файл");
    else
        puts("Ошибка.");
}

void print_help(char* cmd_name) {
    printf(
        "Использование: %s <input> <output>\n"
        "input     Файл, к которому надо применить фильтр\n"
        "output    Файл, в который надо записать результат\n",
        cmd_name
    );
}
